﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections.Specialized;
using MoreSource.Helpers;
using System.Reflection;

namespace MoreSource
{
    class Program
    {
        //Independent helpers
        private static XmlHelper xmlHelper = new XmlHelper();
        private static FileHelper fileHelper = new FileHelper();

        //Configuration
        private static List<ConfigOption> programOptions = xmlHelper.GetProgramOptions();
        private static bool showExceptionData = Convert.ToBoolean(programOptions.Where(option => option.configOptionName.Equals("show_exception_data")).Single().configOptionValue);
        private static bool showDebugData = Convert.ToBoolean(programOptions.Where(option => option.configOptionName.Equals("show_debug_data")).Single().configOptionValue);

        //Dependent helpers
        private static LogHelper logHelper = new LogHelper(
            Convert.ToBoolean(programOptions.Where(option => option.configOptionName.Equals("allow_logging_to_disk")).Single().configOptionValue),
            programOptions.Where(option => option.configOptionName.Equals("log_file_directory")).Single().configOptionValue);

        /// <summary>
        /// Main program thread
        /// Args[0] : Parameters fed in from SourceTree; [0] : Function ID, [1] : $REPO param, [2] : $FILE param, [3] : Config ID
        /// </summary>
        /// <param name="args"></param>
        [STAThread]
        static void Main(string[] args)
        {
            if(showDebugData)
            {
                for(int i = 0; i < args.Length; i++)
                {
                    //Display all arguments
                    Console.WriteLine($"MoreSource: Argument {i} Value: {args[i]}");
                }
                foreach (ConfigOption option in programOptions)
                {
                    //Display all found options
                    Console.WriteLine($"MoreSource: Option Name: {option.configOptionName}\n\tOption Innertext: {option.configOptionValue}\n\tOption ID: {option.configOptionId}");
                }
            }
            
            //Initialize the process
            ProcessStartInfo endProcess;
            try
            {
                //Action based on function ID
                switch(args[0])
                {
                    case "0": //Show File in Explorer

                        if(showDebugData)
                        {
                            Console.WriteLine("MoreSource: Executing: SHOW IN EXPLORER");
                        }
                        endProcess = new ProcessStartInfo(@"C:/Windows/Explorer.exe", $"\"{ReturnRepoFileDirectoryPath(args[1], args[2])}\""); //Inject quotes w/ escapes for path param
                        endProcess.CreateNoWindow = true;
                        Process.Start(endProcess);
                        break;

                    case "1": //Copy to Preset Directory

                        if(showDebugData)
                        {
                            Console.WriteLine("MoreSource: Executing: COPY TO PRESET DIRECTORY");
                        }

                        //Identify the target directory from configuration, init process
                        string configuredTargetDirectory = programOptions.Where(
                            option => option.configOptionName.Equals("copy_to_directory")
                            && option.configOptionId.Equals(Convert.ToInt32(args[3]))).Single().configOptionValue;

                        //Perform copy
                        endProcess = new ProcessStartInfo("cmd.exe", $"/c copy \"{ReturnRepoFilePath(args[1], args[2])}\" \"{configuredTargetDirectory}\" /y"); //Inject quotes w/ escapes for path param
                        endProcess.CreateNoWindow = true;
                        Process.Start(endProcess);
                        break;

                    case "2": //Copy to Clipboard

                        if(showDebugData)
                        {
                            Console.WriteLine("MoreSource: Executing: COPY TO CLIPBOARD");
                        }
                        Clipboard.SetFileDropList(new StringCollection { ReturnRepoFilePath(args[1], args[2]) });
                        break;

                    case "3": //Copy Contents to Clipboard

                        if (showDebugData)
                        {
                            Console.WriteLine("MoreSource: Executing: COPY CONTENTS TO CLIPBOARD");
                        }

                        //Validate extension of file against config; only copy if valid
                        bool fileTypeIsValid = false;
                        foreach(ConfigOption option in programOptions)
                        {
                            if(option.configOptionValue.Equals(ReturnRepoFileType(args[2])))
                            {
                                fileTypeIsValid = true;
                                break;
                            }
                        }
                        if(fileTypeIsValid)
                        {
                            Clipboard.SetText(fileHelper.ReturnAllFileContents(ReturnRepoFilePath(args[1], args[2])));
                        }
                        else
                        {
                            if(showDebugData)
                            {
                                Console.WriteLine($"Copy contents ignored - file extension [{ReturnRepoFileType(args[2])}] is not valid.");
                            }
                            MessageBox.Show("The contents could not be copied - the file extension is not configured as a valid type.");
                            logHelper.WriteToLog("Program", $"Copy contents ignored - file extension [{ReturnRepoFileType(args[2])}] is not valid.");
                        }
                        break;

                    default: //No action
                        logHelper.WriteToLog("Program", "Default case hit - no action taken.");
                        break;
                }

                //Execution complete
                if(showDebugData)
                {
                    Console.WriteLine("MoreSource: COMPLETE");
                }
                Environment.Exit(0);        
            }
            catch (Exception exception)
            {
                //Report error and fail gracefully
                if(showExceptionData)
                {
                    Console.WriteLine($"MoreSource: Operation FAILED - {exception.ToString()}");
                }
                logHelper.WriteToLog("Program", $"Operation FAILED - {exception.ToString()}");
                Environment.Exit(1);
            }
        }

        /// <summary>
        /// Return the absolute path of the selected file item's parent directory, in a command-prompt usable format
        /// </summary>
        /// <param name="repoPath"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private static string ReturnRepoFileDirectoryPath(string repoPath, string fileName)
        {
            string endPath = string.Empty;
            string fullPath = repoPath + "\\" + fileName;
            int dirIndex = 0;

            for (int i = fullPath.Length; i --> 0;)
            {
                //Get the start of the folder structure above the selected file
                if (fullPath[i].Equals('/') || fullPath[i].Equals('\\'))
                {
                    dirIndex = i;
                    break;
                }
            }
            for (int i = 0; i < dirIndex; i++)
            {
                //Build the path to target in explorer, from the root drive up to the last subdirectory
                endPath += fullPath[i];
            }

            //Sanitize and return path
            endPath = endPath.Replace('/', '\\');
            return endPath;
        }

        /// <summary>
        /// Return the absolute path of the selected file item
        /// </summary>
        /// <param name="repoPath"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private static string ReturnRepoFilePath(string repoPath, string fileName)
        {
            return (repoPath + "\\" + fileName).Replace('/', '\\');
        }

        /// <summary>
        /// Return the true name of a file referenced via $FILE by Sourcetree
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private static string ReturnRepoFileName(string fileName)
        {
            if(fileName.Contains("/"))
            {
                //The last substring will be the file itself
                return fileName.Split('/').Last();
            }
            else
            {
                return fileName; 
            }
        }

        /// <summary>
        /// Return the file extension for the given file referenced via $FILE by Sourcetree
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private static string ReturnRepoFileType(string fileName)
        {
            return fileName.Split('.').Last();
        }
    }
}
