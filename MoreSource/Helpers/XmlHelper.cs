﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Reflection;
using MoreSource.Helpers;
using System.Windows.Forms;

namespace MoreSource
{
    class XmlHelper
    {
        private XmlDocument configuration = new XmlDocument();

        /// <summary>
        /// Constructor
        /// </summary>
        public XmlHelper()
        {
            try
            {
                //Attempt to load in XML configuration data
                string appHostDirectory = Assembly.GetEntryAssembly().Location;
                appHostDirectory = appHostDirectory.Replace("MoreSource.exe", @"Resources\configuration.xml");
                Console.WriteLine($"MoreSource: Entry Assembly: {appHostDirectory}");
                configuration.Load(appHostDirectory);
            }
            catch (Exception exception)
            {
                //Missing configuration files - terminate
                Console.WriteLine($"MoreSource: FAILED to load resources!\nException data: {exception.ToString()}\nMoreSauce will now exit.");
                MessageBox.Show("The configuration file could not be found - MoreSource will now exit.");
                Environment.Exit(1);
            }
        }

        /// <summary>
        /// Find and return all program options, and values, as a dictionary item
        /// </summary>
        /// <returns></returns>
        public List<ConfigOption> GetProgramOptions()
        {
            List<ConfigOption> programOptions = new List<ConfigOption>();
            foreach(XmlNode node in configuration.GetElementsByTagName("options")[0].ChildNodes)
            {
                try
                {
                    //Skip comments; attempt to deduce option from node
                    if(!node.NodeType.Equals(XmlNodeType.Comment))
                    {
                        XmlElement nodeToElement = (XmlElement)node; //We can't evaluate missing attributes on XmlNode
                        programOptions.Add(new ConfigOption(
                            node.Name,
                            node.InnerText,
                            nodeToElement.HasAttribute("id") ? Convert.ToInt32(node.Attributes["id"].Value) : -1));
                    }                    
                }
                catch
                {
                    //Invalid node for option conversion - skip it
                    Console.WriteLine("MoreSource: Skipped invalid option: " + node.Name);
                }
            }
            return programOptions;
        }
    }
}