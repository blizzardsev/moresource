﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MoreSource.Helpers
{
    class FileHelper
    {
        private StreamReader reader;

        //Constructor
        public FileHelper()
        {
            
        }

        /// <summary>
        /// Return the content of a target file as a list of strings
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public string ReturnAllFileContents(string filePath)
        {
            reader = new StreamReader(filePath);
            string fileContent = String.Empty;

            while(!reader.EndOfStream)
            {
                fileContent += $"{reader.ReadLine()}\n";
            }
            reader.Close();
            reader.Dispose();
            return fileContent;
        }
    }
}
