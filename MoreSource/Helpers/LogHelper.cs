﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace MoreSource.Helpers
{
    class LogHelper
    {
        private StreamWriter writer;
        private bool writeAccessToLog;
        private string logFileDirectory;

        /// <summary>
        /// Constructor
        /// </summary>
        public LogHelper(bool writeAccess, string logDir)
        {
            try
            {
                writeAccessToLog = writeAccess;
                logFileDirectory = $"{logDir}MoreSource\\log.txt";

                if(writeAccessToLog)
                {
                    VerifyLog(logDir);
                    writer = new StreamWriter(File.Open(logFileDirectory, FileMode.Append));
                }
                else
                {
                    Console.WriteLine("MoreSource: Log file write access was denied. Logging will not be active for this session.");
                }
            }
            catch(Exception exception)
            {
                //Missing log file - terminate
                MessageBox.Show("The log file could not be found - MoreSource will now exit.");
                Console.WriteLine($"MoreSource: FAILED to load log file!\nException data: {exception.ToString()}\nMoreSauce will now exit.");
                Environment.Exit(1);
            }
        }

        /// <summary>
        /// Verify if the log exists; create at the configured directory if not
        /// </summary>
        /// <param name="logDir"></param>
        public void VerifyLog(string logDir)
        {
            //Account for pasted Explorer.exe directories
            logDir = logDir.Last().Equals('\\') ? logDir : $"{logDir}\\";
            if(File.Exists($"{logDir}MoreSource\\log.txt"))
            {
                //Verified
                return;
            }
            else
            {
                //Create the log
                Directory.CreateDirectory($"{logDir}MoreSource");
                File.Create($"{logDir}MoreSource\\log.txt").Close();
                Console.WriteLine("MoreSource: Log created.");
            }
        }

        /// <summary>
        /// Write exception data to the program log
        /// </summary>
        /// <param name="process"></param>
        /// <param name="exception"></param>
        public void WriteToLog(string process, string message)
        {
            if(writeAccessToLog)
            {
                //Append using stream
                string logEntry = String.IsNullOrEmpty(process) ? message : $"{process} - {message}";
                writer.WriteLine($"MoreSource: {logEntry}\n\t{DateTime.Now.ToShortDateString()} @ {DateTime.Now.ToShortTimeString()}\n");   
            }
        }
    }
}
