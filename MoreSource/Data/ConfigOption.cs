﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoreSource
{
    class ConfigOption
    {
        //Configuration item attributes
        public string configOptionName;
        public string configOptionValue;
        public int? configOptionId;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="optionName"></param>
        /// <param name="optionValue"></param>
        /// <param name="optionId"></param>
        public ConfigOption(string optionName, string optionValue, int optionId)
        {
            //Pulled from configuration.xml
            this.configOptionName = optionName;
            this.configOptionValue = optionValue;
            this.configOptionId = optionId;
        }
    }
}
